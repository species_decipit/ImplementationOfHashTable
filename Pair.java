import java.util.Map;

public class Pair<K, V> implements Map.Entry<K, V>{
    private K _key;
    private V _value;

    Pair(K key, V value) {
        setKey(key);
        setValue(value);
    }

    @Override
    public K getKey() { return _key; }

    @Override
    public V getValue() { return _value; }

    @Override
    public V setValue(V value) {
        V buf = getValue();
        _value = value;
        return buf;
    }

    public String toString() {  return "(" + getKey() + ", " + getValue() + ")"; }

    private void setKey(K key) { _key = key; }
}
